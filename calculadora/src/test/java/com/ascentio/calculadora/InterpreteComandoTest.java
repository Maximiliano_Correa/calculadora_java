package com.ascentio.calculadora;

import com.ascentio.calculadora.dao.repository.ComandoRepository;
import com.ascentio.calculadora.service.InterpreteComando;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InterpreteComandoTest {

    @InjectMocks
    private InterpreteComando interpreteComando;

    @Mock
    private ComandoRepository comandoRepository;

    @Test
    public void insertExpresionSuma() {
        String expresion = "2+2";
        String resultado = "4";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(resultado));
    }

    @Test
    public void insertExpresionResta() {
        String expresion = "2-2";
        String resultado = "0";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(resultado));
    }

    @Test
    public void insertExpresionMultiplicacion() {
        String expresion = "2*2";
        String resultado = "4";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(resultado));
    }

    @Test
    public void insertExpresionDivision() {
        String expresion = "10/2";
        String resultado = "5";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(resultado));
    }

    @Test
    public void insertExpresionLogaritmoNeperiano() {
        String expresion = "log(5)";
        String resultado = "1.609438";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(resultado));
    }

    @Test
    public void insertExpresionExponente() {
        String expresion = "2^2";
        String resultado = "4";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(resultado));
    }

    @Test
    public void insertExpresionCompleta() {
        String expresion = "3 + 4 - log(23.2) ^ (2-1) * -1";
        String resultado = "10.14415";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(resultado));
    }

    @Test
    public void insertExpresionRestaurarSinSesion() {
        String expresion = "recuperar sesion1";
        String error = "ERROR";
        String sesion = "sesion1";
        when(comandoRepository.existsBySesion(sesion)).thenReturn(false);
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(error));
    }

    @Test
    public void insertExpresionGuardarAlternado() {
        String expresion = "sesion1 guardar";
        String error = "ERROR";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(error));
    }

    @Test
    public void insertExpresionGuardarVacio() {
        String expresion = "guardar";
        String error = "ERROR";
        assert (interpreteComando.analisisComando(expresion).getResultado().get(0).equals(error));
    }


}