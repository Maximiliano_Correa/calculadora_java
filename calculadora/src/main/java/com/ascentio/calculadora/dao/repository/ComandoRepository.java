package com.ascentio.calculadora.dao.repository;

import com.ascentio.calculadora.dao.model.ComandoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface ComandoRepository extends CrudRepository<ComandoEntity, Integer> {

    List<ComandoEntity> findAllBySesion(@Param("nombreSesion") String nombreSesion);

    boolean existsBySesion(@Param("nombreSesion") String nombreSesion);
}

