package com.ascentio.calculadora.controller.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"resultado"})
public class Resultado {

    @JsonProperty("resultado")
    private List<String> resultado;

    @JsonProperty("resultado")
    public List<String> getResultado() {
        return resultado;
    }

    @JsonProperty("resultado")
    public void setResultado(List<String> resultado) {
        this.resultado = resultado;
    }

    @Override
    public String toString() {
        return "Resultado{" +
                "resultado=" + resultado +
                '}';
    }
}

