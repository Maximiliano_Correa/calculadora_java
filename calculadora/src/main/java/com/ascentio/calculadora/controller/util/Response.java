package com.ascentio.calculadora.controller.util;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Response<T> {
    @JsonProperty("status")
    private boolean status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("errors")
    private List<String> errors;
    @JsonProperty("data")
    private T data;

    public Response(){

    }

    public Response(boolean status, String message, List<String> errors, T data) {
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.data = data;
    }

    public Response(boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public boolean isStatus()
    {
        return status;
    }
    public void setStatus(boolean status)
    {
        this.status = status;
    }
    public String getMessage()
    {
        return message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }

    public List<String> getErrors()
    {
        if(errors == null){
            errors = new ArrayList<String>();
        }
        return errors;
    }
    public void setErrors(List<String> errors)
    {
        this.errors = errors;
    }

    public void addError(String error)
    {
        getErrors().add(error);
    }
    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }

}
