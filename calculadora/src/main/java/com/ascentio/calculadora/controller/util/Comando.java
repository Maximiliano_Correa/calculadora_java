package com.ascentio.calculadora.controller.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({"comando"})

    public class Comando {
        @JsonProperty("comando")
        private String comando;

        @JsonProperty("comando")
        public String getComando() {
            return comando;
        }

        @JsonProperty("comando")
        public void setComando(String comando) {
            this.comando = comando;
        }

        @Override
        public String toString() {
            return "Comando{" +
                    "comando='" + comando + '\'' +
                    '}';
        }

    }

