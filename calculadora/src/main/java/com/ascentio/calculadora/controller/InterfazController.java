package com.ascentio.calculadora.controller;

import com.ascentio.calculadora.controller.util.Comando;
import com.ascentio.calculadora.controller.util.Message;
import com.ascentio.calculadora.controller.util.Response;
import com.ascentio.calculadora.service.InterpreteComando;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/calculadora/v1.0")
public class InterfazController {

    private final String INTERFAZ = "/interfaz";

    @Autowired
    private InterpreteComando interpreteComando;

    //--------------------------------------------------------------------------------------------------
    @RequestMapping(value = INTERFAZ , method=RequestMethod.POST)
    public ResponseEntity<Response> postCalculo(@RequestBody Comando input){
        Response msg = new Message(true, INTERFAZ);
        msg.setData(interpreteComando.analisisComando(input.getComando().toLowerCase().trim()));
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }
    //--------------------------------------------------------------------------------------------------

}
