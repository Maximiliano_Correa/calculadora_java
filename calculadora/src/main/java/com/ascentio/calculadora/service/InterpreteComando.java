package com.ascentio.calculadora.service;

import com.ascentio.calculadora.controller.util.Resultado;
import com.ascentio.calculadora.dao.model.ComandoEntity;
import com.ascentio.calculadora.dao.repository.ComandoRepository;
import com.udojava.evalex.Expression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class InterpreteComando {

    @Autowired
    private ComandoRepository comandoRepository;

    private List<ComandoEntity> buffer = new LinkedList<>();

    public Resultado analisisComando(String expMatematica) {
        final String RECUPERAR = "recuperar";
        final String GUARDAR = "guardar";
        final String ALMACENADA = " almacenada";
        final String ERROR = "ERROR";
        List<String> outPut = new LinkedList<>();
        ComandoEntity comandoEntity = new ComandoEntity();
        Resultado resultado = new Resultado();
        String[] msg = expMatematica.split(" ", 2);

        if (esComandoInterno(expMatematica, msg, GUARDAR)) {
            String mensaje = msg[1] + ALMACENADA;
            guardarSesion(msg[1]);
            outPut.clear();
            outPut.add(mensaje);
        } else {
            if (esComandoInterno(expMatematica, msg, RECUPERAR)) {
                String nombreSesion = msg[1];
                outPut = recuperarSesion(nombreSesion);
            } else {
                outPut.clear();
                try {
                    Expression expression = new Expression(expMatematica);
                    BigDecimal bigDecimal = expression.eval();
                    outPut.add(String.valueOf(bigDecimal));
                    comandoEntity.setInput(expMatematica);
                    comandoEntity.setOutput(String.valueOf(bigDecimal));
                    buffer.add(comandoEntity);
                } catch (Expression.ExpressionException e) {
                    outPut.add(ERROR);
                }
            }
        }

        resultado.setResultado(outPut);
        return resultado;
    }

    private List<String> recuperarSesion(String nombreSesion) {
        if (!comandoRepository.existsBySesion(nombreSesion)) {
            List<String> outPut = new LinkedList<>();
            outPut.add("ERROR");
            return outPut;
        }
        return comandoRepository.findAllBySesion(nombreSesion).stream().map(cmd -> cmd.getInput() + " = " + cmd.getOutput()).collect(Collectors.toList());
    }

    private void guardarSesion(String sesion) {
        buffer.forEach(exp -> {
            exp.setSesion(sesion);
            comandoRepository.save(exp);
        });
        buffer.clear();
    }

    private boolean esComandoInterno(String expMatematica, String[] msg, String comando) {
        return expMatematica.contains(comando) && (msg.length > 1) && (msg[0].equals(comando));
    }

}
